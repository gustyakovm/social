<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Session;

class ProfileController extends Controller
{
    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();
        return view('profile.profile', compact('user'));
    }

    public function edit()
    {
        $info = auth()->user()->profile;
        return view('profile.edit', compact('info'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'location' => 'required',
            'about' => 'required|max:200'
        ]);

        auth()->user()->profile()->update([
            'location' => $request->get('location'),
            'about' => $request->get('about')
        ]);

        if($request->hasFile('avatar')){
            auth()->user()->update([
                'avatar' => $request->avatar->store('public/avatars')
            ]);
        }

        Session::flash('success', 'Profile updated');
        return back();
    }



}
