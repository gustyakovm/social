<?php

namespace App\Http\Controllers;

use App\Notifications\FriendRequestAccepted;
use App\Notifications\NewFriendRequest;
use App\User;
use Illuminate\Http\Request;

class FriendshipsController extends Controller
{

    public function check($id)
    {
        if(auth()->user()->is_friends_with($id)){
            return ['status' => 'friends'];
        }

        if(auth()->user()->has_pending_friend_request_from($id)){
            return ['status' => 'pending'];
        }

        if(auth()->user()->has_pending_friend_request_sent_to($id)){
            return ['status' => 'waiting'];
        }

        return ['status' => 0];
    }


    public function add_friend($id)
    {
        $resp = auth()->user()->add_friend($id);
        User::find($id)->notify(new NewFriendRequest(auth()->user()));
        return $resp;
    }


    public function accept_friend($id)
    {
        $resp = auth()->user()->accept_friend($id);
        User::find($id)->notify(new FriendRequestAccepted(auth()->user()));
        return $resp;

    }

}
