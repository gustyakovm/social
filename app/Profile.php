<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ["location" , "about", "user_id"];

    /**
     * @return array
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
