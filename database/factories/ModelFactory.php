<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $name = $faker->unique()->name;
    return [
        'name' => $name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('12345678'),
        'gender' => 0,
        'avatar' => 'public/avatars/female.png',
        'remember_token' => str_random(10),
        'slug' => str_slug($name)
    ];
});

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
      'location' => $faker->city,
      'about'   => $faker->paragraph(4)
    ];
});
