@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $user->name  }}'s profile.</div>
                <div class="panel-body">
                    <p class="text-center">
                        <img width="150px" src="{{ $user->avatar }}" alt="">
                    </p>

                    <p class="text-center">
                        @if($user->id == auth()->id())
                            <a href="{{ route('profile.edit') }}" class="btn btn-info">Edit your profile</a>
                        @endif
                    </p>

                    <hr>
                    <p class="text-center">
                        {{ $user->profile->location  }}
                    </p>
                    <p class="text-center">
                        {{ $user->profile->about  }}
                    </p>

                </div>
            </div>


            @if(auth()->id() != $user->id)
                <div class="panel panel-default">
                    <div class="panel-heading">Friends</div>
                    <div class="panel-body">
                        <friend :profile_user_id="{{ $user->id }}"></friend>
                    </div>
                </div>
            @endif


        </div>

    </div>
@stop