@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Edit your profile</div>
                <div class="panel-body">
                    <form action="{{ route('profile.update')  }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <p class="text-center">
                            <img width="150px" src="{{ auth()->user()->avatar }}" alt="">
                        </p>

                        <div class="form-group">
                            <label for="avatar">Upload avatar</label>
                            <input type="file" name="avatar" accept="image/*" >
                        </div>

                        <div class="form-group">
                            <label for="location">Location</label>
                            <input type="text" name="location" value="{{ $info->location }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="about">About</label>
                            <textarea name="about" id="" cols="30" rows="10" class="form-control">{{ $info->about }}</textarea>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-block btn-primary" value="Save">
                        </div>


                    </form>
                </div>
            </div>
        </div>

    </div>
@stop