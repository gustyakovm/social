@if(Session::has('success'))
    <script>
        noty({
            'type' : 'success',
            'layout' : 'bottomRight',
            'text' : "{{ Session::get('success') }}",
            'timeout' : 4000
        });
    </script>
@endif